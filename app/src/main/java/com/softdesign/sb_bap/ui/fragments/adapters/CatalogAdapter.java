package com.softdesign.sb_bap.ui.fragments.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.softdesign.sb_bap.data.storage.dto.ProductDTO;
import com.softdesign.sb_bap.ui.fragments.ProductFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apborezkiy on 03.11.16.
 */

public class CatalogAdapter extends FragmentPagerAdapter {
    private List<ProductDTO> mProductDTOList = new ArrayList<>();

    public CatalogAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return ProductFragment.newInstance(mProductDTOList.get(position));
    }

    @Override
    public int getCount() {
        return mProductDTOList.size();
    }

    public void addItem(ProductDTO productDTO) {
        mProductDTOList.add(productDTO);
        notifyDataSetChanged();
    }
}
