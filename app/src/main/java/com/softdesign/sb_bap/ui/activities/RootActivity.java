package com.softdesign.sb_bap.ui.activities;

import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.softdesign.sb_bap.BuildConfig;
import com.softdesign.sb_bap.R;
import com.softdesign.sb_bap.data.managers.DataManager;
import com.softdesign.sb_bap.data.storage.dto.ProductDTO;
import com.softdesign.sb_bap.mvp.views.ICatalogView;
import com.softdesign.sb_bap.mvp.views.IView;
import com.softdesign.sb_bap.ui.fragments.AccountFragment;
import com.softdesign.sb_bap.ui.fragments.CatalogFragment;
import com.softdesign.sb_bap.ui.fragments.ProductFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RootActivity extends AppCompatActivity implements ICatalogView, NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawer;
    @BindView(R.id.nav_view)
    NavigationView mNavView;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.coordinator_container)
    CoordinatorLayout mCoordinatorLayout;
    @BindView(R.id.fragment_container)
    FrameLayout mFragmentContainer;

    FragmentManager mFragmentManager;

    //region ======= life cycle =======

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        ButterKnife.bind(this);

        initToolbar();
        initDrawer();

        mFragmentManager = getSupportFragmentManager();
        if (savedInstanceState == null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, new CatalogFragment())
                    .commit();
        }
    }
    
    //endregion
    
    //region ======= IView =======

    @Override
    public void showMessage(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showError(Throwable e) {
        if (BuildConfig.DEBUG) {
            showMessage(e.getMessage());
            e.printStackTrace();
        } else {
            showMessage(getString(R.string.error));
            // TODO: send error report to crashlytics
        }
    }

    @Override
    public void showLoad() {
        // TODO: show load progress
    }

    @Override
    public void hideLoad() {
        // TODO: hide load progress
    }

    //endregion

    //region ======= ICatalogView =======

    @Override
    public void showAddToCartMessage(ProductDTO productDTO) {
        showMessage("Товар " + productDTO.getProductName() + " успешно добавлен в корзину");
    }

    @Override
    public void showCatalogView(List<ProductDTO> productDTOList) {

    }

    @Override
    public void showAuthScreen() {

    }

    @Override
    public void updateProductCounter() {

    }

    //endregion

    private void initDrawer() {
        setSupportActionBar(mToolbar);
    }

    private void initToolbar() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.open_drawer, R.string.close_drawer);
        mDrawer.setDrawerListener (toggle);
        toggle.syncState();
        mNavView.setNavigationItemSelectedListener(this);

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        Fragment fragment = null;
        switch (item.getItemId ()) {
            case R.id.nav_account:
                fragment = new AccountFragment();
                break;
            case R.id.nav_catalog:
                fragment = new CatalogFragment();
                break;
            case R.id.nav_favorite:
                break;
            case R.id.nav_orders:
                break;
            case R.id.nav_notifications:
                break;
        }
        if (fragment != null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .addToBackStack(null)
                    .commit();
        }

        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
