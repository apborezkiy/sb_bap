package com.softdesign.sb_bap.ui.fragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.softdesign.sb_bap.R;
import com.softdesign.sb_bap.data.storage.dto.ProductDTO;
import com.softdesign.sb_bap.mvp.presenters.ProductPresenter;
import com.softdesign.sb_bap.mvp.presenters.ProductPresenterFactory;
import com.softdesign.sb_bap.mvp.views.IProductView;
import com.softdesign.sb_bap.ui.activities.RootActivity;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by apborezkiy on 03.11.16.
 */

public class ProductFragment extends Fragment implements IProductView, View.OnClickListener {
    private static final String TAG = "ProductFragment";

    @BindView(R.id.product_name_txt)
    TextView mProductNameTxt;
    @BindView(R.id.product_description_txt)
    TextView mProductDescriptionTxt;
    @BindView(R.id.product_image)
    ImageView mProductImage;
    @BindView(R.id.product_count_txt)
    TextView mProductCountTxt;
    @BindView(R.id.product_price_txt)
    TextView mProductPriceTxt;
    @BindView(R.id.plus_btn)
    ImageButton mPlusBtn;
    @BindView(R.id.minus_btn)
    ImageButton mMinusBtn;

    // TODO: 03.11.16 it is temp solution - fix it (picasso)
    @BindDrawable(R.drawable.product)
    Drawable mProductDraw;

    private ProductPresenter mPresenter;

    public static ProductFragment newInstance(ProductDTO product) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("PRODUCT", product);
        ProductFragment fragment = new ProductFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    private void readBundle(Bundle bundle) {
        if (bundle != null) {
            ProductDTO product = bundle.getParcelable("PRODUCT");
            mPresenter = ProductPresenterFactory.getInstance(product);
        }
    }

    //region ======= Life cycle =======

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_product, container, false);
        ButterKnife.bind(this, view);
        readBundle(getArguments());
        mPresenter.takeView(this);
        mPresenter.initView();
        mPlusBtn.setOnClickListener(this);
        mMinusBtn.setOnClickListener(this);
        return view;
    }

    public void onDestroyView() {
        mPresenter.dropView();
        super.onDestroyView();
    }

    //endregion

    //region ======= IProductView =======

    @Override
    public void showProductView(ProductDTO product) {
        mProductNameTxt.setText(product.getProductName());
        mProductDescriptionTxt.setText(product.getDescription());
        mProductCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            mProductPriceTxt.setText(String.valueOf(product.getCount() * product.getPrice() + ".-"));
        } else {
            mProductPriceTxt.setText(String.valueOf(product.getPrice()) + ".-");
        }
        // TODO: 03.11.16 Picasso load from url
        mProductImage.setImageDrawable(mProductDraw);
    }

    @Override
    public void updateProductCountView(ProductDTO product) {
        mProductCountTxt.setText(String.valueOf(product.getCount()));
        if (product.getCount() > 0) {
            mProductPriceTxt.setText(String.valueOf(product.getCount() * product.getPrice() + ".-"));
        }
    }

    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showError(Throwable e) {
        getRootActivity().showError(e);
    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getRootActivity().hideLoad();
    }

    //endregion

    private RootActivity getRootActivity() {
        return (RootActivity) getActivity();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.minus_btn:
                mPresenter.clickOnMinus();
                break;
            case R.id.plus_btn:
                mPresenter.clickOnPlus();
                break;
        }
    }
}
