package com.softdesign.sb_bap.ui.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.design.widget.CoordinatorLayout;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.softdesign.sb_bap.BuildConfig;
import com.softdesign.sb_bap.R;
//import com.softdesign.sb_bap.databinding.ActivityRootBinding;
import com.softdesign.sb_bap.mvp.presenters.AuthPresenter;
import com.softdesign.sb_bap.mvp.views.IAuthView;
import com.softdesign.sb_bap.ui.custom_views.AuthPanel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity implements IAuthView, View.OnClickListener {
  AuthPresenter mPresenter = AuthPresenter.getInstance();
  //ActivityRootBinding binding;

  @BindView(R.id.activity_splash_root) CoordinatorLayout mCoordinatorLayout;
  @BindView(R.id.auth_panel) AuthPanel mAuthPanel;
  @BindView(R.id.login_btn) Button mLoginBtn;
  @BindView(R.id.show_catalog_btn) Button mShowCatalogBtn;

  //region ======= life cycle =======

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash);

    ButterKnife.bind(this);
    //ActivityRootBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_root);
    //binding.setClicker(this);

    this.setupFonts();

    mLoginBtn.setOnClickListener(this);
    mShowCatalogBtn.setOnClickListener(this);

    mPresenter.takeView(this);
    mPresenter.initView();
  }

  @Override
  protected void onDestroy() {
    mPresenter.dropView();
    super.onDestroy();
  }

  //endregion

  //region ======= IAuthView =======

  @Override
  public AuthPanel getAuthPanel() {
    return mAuthPanel;
  }

  @Override
  public AuthPresenter getPresenter() {
    return mPresenter;
  }

  @Override
  public void showLoginBtn() {
    mLoginBtn.setVisibility(View.VISIBLE);
  }

  @Override
  public void hideLoginBtn() {
    mLoginBtn.setVisibility(View.GONE);
  }

  @Override
  public void showCatalogScreen() {
    Intent intent = new Intent(this, RootActivity.class);
    startActivity(intent);
  }

  //endregion

  //region ======= IVew =======

  @Override
  public void showMessage(String message) {
    Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
  }

  @Override
  public void showError(Throwable e) {
    if (BuildConfig.DEBUG) {
      showMessage(e.getMessage());
      e.printStackTrace();
    } else {
      showMessage(getString(R.string.error));
      // TODO: send error report to crashlytics
    }
  }

  @Override
  public void showLoad() {
    // TODO: show load progress
  }

  @Override
  public void hideLoad() {
    // TODO: hide load progress
  }

  //endregion

  private void setupFonts() {
    Typeface keys = Typeface.createFromAsset(getAssets(), getString(R.string.regular_font));
    TextView key1 = (TextView)findViewById(R.id.login_email_et);
    key1.setTypeface(keys);
    TextView key2 = (TextView)findViewById(R.id.login_password_et);
    key2.setTypeface(keys);
  }

  @Override
  public void onClick(View view) {
    switch (view.getId()) {
      case R.id.show_catalog_btn:
        mPresenter.clickOnShowCatalog();
        break;
      case R.id.login_btn:
        mPresenter.clickOnLogin();
        break;
    }
  }

  @Override
  public void onBackPressed() {
    if (!mAuthPanel.isIdle()) {
      mAuthPanel.setCustomState(AuthPanel.IDLE_STATE);
    } else {
      super.onBackPressed();
    }
  }
}
