package com.softdesign.sb_bap.ui.custom_views;

import android.content.Context;
import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.softdesign.sb_bap.R;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Энергичный Здоровый on 25.10.2016.
 */

public class AuthPanel extends LinearLayout implements TextWatcher {

  private static final String TAG = "AuthPanel";
  public static final int LOGIN_STATE = 0;
  public static final int IDLE_STATE = 1;
  private int mCustomState = 1;
  private boolean emailValid = false;
  private boolean passwordValid = false;

  @Override
  public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

  }

  @Override
  public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

  }

  @Override
  public void afterTextChanged(Editable editable) {
    Editable email = mEmailEt.getEditableText();
    Editable password = mPasswordEt.getEditableText();
    if (email == editable) {
      Pattern p = Pattern.compile("^([a-zA-Z0-9]+@[a-zA-Z0-9]+.[a-zA-Z0-9]+)$");
      emailValid = p.matcher(mEmailEt.getText()).matches();
      if (emailValid) {
        mEmailEt.setTextColor(Color.BLACK);
      } else {
        mEmailEt.setTextColor(Color.RED);
      }
    }
    if (password == editable) {
      Pattern p = Pattern.compile("^([a-zA-Z0-9]{8,})$");
      passwordValid = p.matcher(mPasswordEt.getText()).matches();
      if (passwordValid) {
        mPasswordEt.setTextColor(Color.BLACK);
      } else {
        mPasswordEt.setTextColor(Color.RED);
      }
    }
    if (mCustomState == LOGIN_STATE) {
        if (emailValid && passwordValid) {
          mLoginBtn.setEnabled(true);
        } else {
          mLoginBtn.setEnabled(false);
        }
    }
  }

  public AuthPanel(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  // TODO: validate and save state for email input

  @BindView(R.id.auth_card) CardView mAuthCard;
  @BindView(R.id.login_email_et) EditText mEmailEt;
  @BindView(R.id.login_password_et) EditText mPasswordEt;
  @BindView(R.id.login_btn) Button mLoginBtn;
  @BindView(R.id.show_catalog_btn) Button mShowCatalogBtn;

  @Override
  protected void onFinishInflate() {
    super.onFinishInflate();
    ButterKnife.bind(this);
    showViewFromState();
    mEmailEt.addTextChangedListener(this);
    mPasswordEt.addTextChangedListener(this);
    mLoginBtn.setEnabled(true);
  }

  @Override
  protected Parcelable onSaveInstanceState() {
    Parcelable superState = super.onSaveInstanceState();
    SavedState savedState = new SavedState(superState);
    savedState.state = mCustomState;
    return savedState;
  }

  @Override
  protected void onRestoreInstanceState(Parcelable state) {
    SavedState savedState = (SavedState) state;
    super.onRestoreInstanceState(savedState);
    setCustomState(savedState.state);
  }
  
  public void setCustomState(int state) {
    mCustomState = state;
    showViewFromState();
  }

  private void showLoginState() {
    mAuthCard.setVisibility(VISIBLE);
    mLoginBtn.setEnabled(false);
    mShowCatalogBtn.setVisibility(GONE);
  }

  private void showIdleState() {
    mAuthCard.setVisibility(GONE);
    mLoginBtn.setEnabled(true);
    mShowCatalogBtn.setVisibility(VISIBLE);
  }

  private void showViewFromState() {
    if (mCustomState == LOGIN_STATE) {
      showLoginState();
    } else {
      showIdleState();
    }
  }

  public String getUserEmail() {
    return String.valueOf(mEmailEt.getText());
  }

  public String getUserPassword() {
    return String.valueOf(mPasswordEt.getText());
  }

  public boolean isIdle() {
    return mCustomState == IDLE_STATE;
  }
  
  static class SavedState extends BaseSavedState {

    private int state;

    public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
      public SavedState createFromParcel(Parcel in) { return new SavedState(in); }
      public SavedState[] newArray(int size) { return new SavedState[size]; }
    };

    public SavedState(Parcelable superState) {
      super(superState);
    }

    private SavedState(Parcel in) {
      super(in);
      state = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
      super.writeToParcel(out, flags);
      out.writeInt(state);
    }
  }
}
