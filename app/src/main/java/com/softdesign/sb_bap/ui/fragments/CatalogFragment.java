package com.softdesign.sb_bap.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.softdesign.sb_bap.R;
import com.softdesign.sb_bap.data.storage.dto.ProductDTO;
import com.softdesign.sb_bap.mvp.presenters.CatalogPresenter;
import com.softdesign.sb_bap.mvp.views.ICatalogView;
import com.softdesign.sb_bap.ui.activities.RootActivity;
import com.softdesign.sb_bap.ui.fragments.adapters.CatalogAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by apborezkiy on 03.11.16.
 */

public class CatalogFragment extends Fragment implements ICatalogView, View.OnClickListener {
    private static final String TAG = "CatalogFragment";
    private CatalogPresenter mPresenter = CatalogPresenter.getInstance();

    @BindView(R.id.add_to_cart_btn)
    Button addToCartBtn;
    @BindView(R.id.product_pager)
    ViewPager productPager;

    public CatalogFragment() {
    }

    //region ======= Life cycle =======

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_catalog, container, false);
        ButterKnife.bind(this, view);
        mPresenter.takeView(this);
        mPresenter.initView();
        addToCartBtn.setOnClickListener(this);
        return view;
    }

    @Override
    public void onDestroyView() {
        mPresenter.dropView();
        super.onDestroyView();
    }

    //endregion

    //region ======= ICatalogView =======

    @Override
    public void showAddToCartMessage(ProductDTO productDTO) {

    }

    @Override
    public void showCatalogView(List<ProductDTO> productDTOList) {
        CatalogAdapter adapter = new CatalogAdapter(getChildFragmentManager());
        for (ProductDTO productDTO : productDTOList) {
            adapter.addItem(productDTO);
        }
        productPager.setAdapter(adapter);
    }

    @Override
    public void showAuthScreen() {
        // TODO: 03.11.16 show auth screen if user not auth
    }

    @Override
    public void updateProductCounter() {
        // TODO: 03.11.16 update count product on cart icon
    }

    //endregion

    //region ======= IView =======

    @Override
    public void showMessage(String message) {
        getRootActivity().showMessage(message);
    }

    @Override
    public void showError(Throwable e) {
        getRootActivity().showError(e);
    }

    @Override
    public void showLoad() {
        getRootActivity().showLoad();
    }

    @Override
    public void hideLoad() {
        getRootActivity().hideLoad();
    }

    //endregion

    private RootActivity getRootActivity() {
         return (RootActivity) getActivity();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.add_to_cart_btn) {
            mPresenter.clickOnByButton(productPager.getCurrentItem());
        }
    }
}
