package com.softdesign.sb_bap.data.storage.dto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Энергичный Здоровый on 01.11.2016.
 */

public class ProductDTO implements Parcelable {
  private int id;
  private String productName;
  private String imageUrl;
  private String description;
  private int price;
  private int count;

  public ProductDTO(int id, String productName, String imageUrl, String description, int price, int count) {
    this.id = id;
    this.productName = productName;
    this.imageUrl = imageUrl;
    this.description = description;
    this.price = price;
    this.count = count;
  }

  //region ======= parcelable =======

  protected ProductDTO(Parcel in) {
    id = in.readInt();
    productName = in.readString();
    imageUrl = in.readString();
    description = in.readString();
    price = in.readInt();
    count = in.readInt();
  }

  public static final Creator<ProductDTO> CREATOR = new Creator<ProductDTO>() {
    @Override
    public ProductDTO createFromParcel(Parcel in) {
      return new ProductDTO(in);
    }

    @Override
    public ProductDTO[] newArray(int size) {
      return new ProductDTO[size];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeInt(id);
    dest.writeString(productName);
    dest.writeString(imageUrl);
    dest.writeString(description);
    dest.writeInt(price);
    dest.writeInt(count);
  }

  //endregion

  //region ======= getters =======

  public int getId() {
    return id;
  }

  public String getProductName() {
    return productName;
  }

  public String getImageUrl() {
    return imageUrl;
  }

  public String getDescription() {
    return description;
  }

  public int getPrice() {
    return price;
  }

  public int getCount() {
    return count;
  }

  public void deleteProduct() {
    count--;
  }

  public void addProduct() {
    count++;
  }

  //endregion
}
