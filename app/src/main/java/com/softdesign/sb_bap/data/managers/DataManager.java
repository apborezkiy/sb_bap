package com.softdesign.sb_bap.data.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.apache.commons.collections4.MultiMap;
import org.apache.commons.collections4.map.MultiValueMap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//import data.network.RestService;
//import data.network.ServiceGenerator;
//import data.network.res.CharacterRes;
//import data.network.res.HouseRes;
//import data.storage.models.CharacterModel;
//import data.storage.models.CharacterModelDao;
//import data.storage.models.DaoSession;
//import data.storage.models.HouseModel;
//import data.storage.models.HouseModelDao;
import retrofit2.Call;
import retrofit2.Response;

import com.softdesign.sb_bap.data.storage.dto.ProductDTO;
import com.softdesign.sb_bap.utils.ConstantManager;
import com.softdesign.sb_bap.utils.NetworkStatusChecker;
import com.softdesign.sb_bap.utils.ThisApplication;

/**
 * Created by apborezkiy on 15.10.16.
 */
public class DataManager implements ConstantManager {
    private static DataManager ourInstance = new DataManager();
    private static String TAG = "Data Manager";

    private List<ProductDTO> mMockProductList;

    public static DataManager getInstance() {
        return ourInstance;
    }

    private Context mContext;
    //private RestService mRestService;
    //private DaoSession mDaoSession;

    private DataManager() {
        mContext = ThisApplication.getContext();
        //mRestService = ServiceGenerator.createService(RestService.class);
        //mDaoSession = ThisApplication.getDaoSession();
        generateMockData();
    }

    public Context getContent () {
        return mContext;
    }

    public ProductDTO getProductById(int id) {
        // TODO: 02.11.2016 load from db
        return mMockProductList.get(id-1);
    }

    public void updateProduct(ProductDTO product) {
        // TODO: 02.11.2016 save to db
    }

    public List<ProductDTO> getProductList() {
        // TODO: 02.11.2016 get product list from anywhere
        return mMockProductList;
    }

    private void generateMockData() {
        mMockProductList = new ArrayList<>();
        mMockProductList.add(new ProductDTO(1, "test1", "imageUrl", "description", 100, 10));
        mMockProductList.add(new ProductDTO(2, "test2", "imageUrl", "description", 200, 20));
        mMockProductList.add(new ProductDTO(3, "test3", "imageUrl", "description", 300, 30));
        mMockProductList.add(new ProductDTO(4, "test4", "imageUrl", "description", 400, 40));
    }

    public boolean isAuthUser() {
        // TODO: 03.11.16 check user auth token in shared preferences
        return false;
    }
}
