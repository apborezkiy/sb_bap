package com.softdesign.sb_bap.data.managers;

import android.content.SharedPreferences;
import android.util.Log;

import com.softdesign.sb_bap.utils.ConstantManager;
import com.softdesign.sb_bap.utils.ThisApplication;

/**
 * Created by apborezkiy on 15.10.16.
 */

public class PreferencesManager {

    private static PreferencesManager ourInstance;
    public static PreferencesManager getInstance() {
        if (ourInstance != null)
            return ourInstance;
        else {
            ourInstance = new PreferencesManager();
            return ourInstance;
        }
    }

    private SharedPreferences mSharedPreferences;

    public PreferencesManager() {
        mSharedPreferences = ThisApplication.getSharedPreferences();
    }

    public String getToken() {
        return mSharedPreferences.getString(ConstantManager.AUTH_TOKEN, null);
    }
}
