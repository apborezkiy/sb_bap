package com.softdesign.sb_bap.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.facebook.stetho.Stetho;

import org.greenrobot.greendao.database.Database;

//import com.softdesign.sb_bap.data.storage.models.DaoMaster;
//import com.softdesign.sb_bap.data.storage.models.DaoSession;

/**
 * Created by apborezkiy on 15.10.16.
 */

public class ThisApplication extends Application {

    private static SharedPreferences sSharedPreferences;
    private static Context sContext;

    //private static DaoSession sDaoSession;

    public static Context getContext () {
        return sContext;
    }

    public static SharedPreferences getSharedPreferences() {
        return sSharedPreferences;
    }

    //public static DaoSession getDaoSession() {
    //    return sDaoSession;
    //}

    @Override
    public void onCreate() {
        super.onCreate();

        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        sContext = getApplicationContext();

        //DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "SB0-BAP-DB");
        //Database db = helper.getWritableDb();
        //sDaoSession = new DaoMaster(db).newSession();

        Stetho.initializeWithDefaults(this);
    }
}
