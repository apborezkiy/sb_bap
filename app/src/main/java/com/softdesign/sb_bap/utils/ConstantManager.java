package com.softdesign.sb_bap.utils;

public interface ConstantManager {
  String IS_FIRST_LAUNCH = "IS_FIRST_LAUNCH";
  String AUTH_TOKEN = "AUTH_TOKEN";
}
