package com.softdesign.sb_bap.mvp.models;

import com.softdesign.sb_bap.data.managers.DataManager;
import com.softdesign.sb_bap.data.storage.dto.ProductDTO;

import java.util.List;

/**
 * Created by apborezkiy on 03.11.16.
 */

public class CatalogModel {
    DataManager mDataManager = DataManager.getInstance();

    public CatalogModel() {
    }

    public List<ProductDTO> getProductList() {
        return mDataManager.getProductList();
    }

    public boolean isUserAuth() {
        return mDataManager.isAuthUser();
    }
}
