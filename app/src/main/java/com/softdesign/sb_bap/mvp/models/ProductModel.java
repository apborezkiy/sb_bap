package com.softdesign.sb_bap.mvp.models;

import com.softdesign.sb_bap.data.managers.DataManager;
import com.softdesign.sb_bap.data.storage.dto.ProductDTO;

/**
 * Created by Энергичный Здоровый on 02.11.2016.
 */

public class ProductModel {
  DataManager mDataManager = DataManager.getInstance();
  public ProductDTO getProductById(int id) {
    return mDataManager.getProductById(id);
  }
  public void updateProduct(ProductDTO product) {
    mDataManager.updateProduct(product);
  }
}
