package com.softdesign.sb_bap.mvp.presenters;

import com.softdesign.sb_bap.data.storage.dto.ProductDTO;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by apborezkiy on 03.11.16.
 */

public class ProductPresenterFactory {
    private static final Map<String, ProductPresenter> sPresenterMap = new HashMap<>();

    private static void registerPresenter(ProductDTO productDTO, ProductPresenter productPresenter) {
        sPresenterMap.put(String.valueOf(productDTO.getId()), productPresenter);
    }

    public static ProductPresenter getInstance(ProductDTO productDTO) {
        ProductPresenter presenter = sPresenterMap.get(String.valueOf(productDTO.getId()));
        if (presenter == null) {
            presenter = ProductPresenter.newInstance(productDTO);
            registerPresenter(productDTO, presenter);
        }
        return presenter;
    }
}
