package com.softdesign.sb_bap.mvp.presenters;

import android.support.annotation.Nullable;

import com.softdesign.sb_bap.data.storage.dto.ProductDTO;
import com.softdesign.sb_bap.mvp.models.AuthModel;
import com.softdesign.sb_bap.mvp.models.CatalogModel;
import com.softdesign.sb_bap.mvp.views.ICatalogView;

import java.util.List;

/**
 * Created by apborezkiy on 03.11.16.
 */

public class CatalogPresenter extends AbstractPresenter<ICatalogView> implements ICatalogPresenter {
    private static CatalogPresenter ourInstance = new CatalogPresenter();
    private CatalogModel mCatalogModel;
    private ICatalogView mICatalogView;

    private List<ProductDTO> mProductDTOList;

    public CatalogPresenter() {
        mCatalogModel = new CatalogModel();
    }

    public static CatalogPresenter getInstance() {
        return ourInstance;
    }

    //region ======= AbstractPresenter =======

    @Override
    public void takeView(ICatalogView view) {
        super.takeView(view);
    }

    @Override
    public void dropView() {
        super.dropView();
    }

    @Override
    public void initView() {
        if (mProductDTOList == null) {
            mProductDTOList = mCatalogModel.getProductList();
        }

        if (getView() != null) {
            getView().showCatalogView(mProductDTOList);
        }
    }

    @Nullable
    @Override
    public ICatalogView getView() {
        return super.getView();
    }

    //endregion
    
    //region ======= ICatalogPresenter =======

    @Override
    public void clickOnByButton(int position) {
        if (getView() != null) {
            if (checkUserAuth()) {
                getView().showAddToCartMessage(mProductDTOList.get(position));
            } else {
                getView().showAuthScreen();
            }
        }
    }

    @Override
    public boolean checkUserAuth() {
        return false;
    }

    //endregion
}
