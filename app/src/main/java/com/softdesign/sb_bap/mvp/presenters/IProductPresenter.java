package com.softdesign.sb_bap.mvp.presenters;

import com.softdesign.sb_bap.mvp.views.IProductView;

/**
 * Created by Энергичный Здоровый on 01.11.2016.
 */

public interface IProductPresenter {

  void clickOnPlus();
  void clickOnMinus();

}
