package com.softdesign.sb_bap.mvp.views;

/**
 * Created by Энергичный Здоровый on 01.11.2016.
 */

public interface IView {
  void showMessage(String message);
  void showError(Throwable e);

  void showLoad();
  void hideLoad();
}
