package com.softdesign.sb_bap.mvp.views;

import com.softdesign.sb_bap.data.storage.dto.ProductDTO;

import java.util.List;

/**
 * Created by apborezkiy on 03.11.16.
 */

public interface ICatalogView extends IView {
    void showAddToCartMessage(ProductDTO productDTO);
    void showCatalogView(List<ProductDTO> productDTOList);
    void showAuthScreen();
    void updateProductCounter();
}
