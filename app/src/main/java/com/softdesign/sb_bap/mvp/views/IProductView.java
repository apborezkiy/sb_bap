package com.softdesign.sb_bap.mvp.views;

import com.softdesign.sb_bap.data.storage.dto.ProductDTO;

/**
 * Created by Энергичный Здоровый on 01.11.2016.
 */

public interface IProductView extends IView {
  void showProductView(ProductDTO product);
  void updateProductCountView(ProductDTO product);
}
