package com.softdesign.sb_bap.mvp.views;

import android.support.annotation.Nullable;

import com.softdesign.sb_bap.mvp.presenters.AuthPresenter;
import com.softdesign.sb_bap.ui.custom_views.AuthPanel;

/**
 * Created by apborezkiy on 25.10.16.
 */

public interface IAuthView extends IView {

    void showMessage(String message);
    void showError(Throwable e);

    void showLoad();
    void hideLoad();

    AuthPresenter getPresenter();

    void showLoginBtn();
    void hideLoginBtn();

    @Nullable
    AuthPanel getAuthPanel();
    
    void showCatalogScreen();
}
