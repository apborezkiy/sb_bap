package com.softdesign.sb_bap.mvp.presenters;

import android.support.annotation.Nullable;

import com.softdesign.sb_bap.mvp.views.IAuthView;

/**
 * Created by apborezkiy on 25.10.16.
 */

public interface IAuthPresenter {

    void takeView(IAuthView authView);
    void dropView();
    void initView();

    @Nullable
    IAuthView getView();

    void clickOnLogin();
    void clickOnFb();
    void clickOnTw();
    void clickOnVk();
    void clickOnShowCatalog();

    boolean checkUserAuth();
}
