package com.softdesign.sb_bap.mvp.presenters;

/**
 * Created by apborezkiy on 03.11.16.
 */

public interface ICatalogPresenter {
    void clickOnByButton(int position);
    boolean checkUserAuth();
}
