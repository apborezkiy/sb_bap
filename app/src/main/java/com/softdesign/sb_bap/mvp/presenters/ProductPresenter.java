package com.softdesign.sb_bap.mvp.presenters;

import com.softdesign.sb_bap.data.storage.dto.ProductDTO;
import com.softdesign.sb_bap.mvp.models.ProductModel;
import com.softdesign.sb_bap.mvp.views.IProductView;

/**
 * Created by apborezkiy on 03.11.16.
 */
public class ProductPresenter extends AbstractPresenter<IProductView> implements IProductPresenter {
    private static final String TAG = "ProductPresenter";
    private ProductModel mProductModel;
    private ProductDTO mProductDTO;

    public static ProductPresenter newInstance(ProductDTO product) {
        return new ProductPresenter(product);
    }

    private ProductPresenter(ProductDTO product) {
        mProductModel = new ProductModel();
        mProductDTO = product;
    }

    //region ======= IProductPresenter =======

    @Override
    public void clickOnPlus() {
        if (mProductDTO.getCount()<99) {
            mProductDTO.addProduct();
            mProductModel.updateProduct(mProductDTO);
            if (getView() != null) {
                getView().updateProductCountView(mProductDTO);
            }
        }
    }

    @Override
    public void clickOnMinus() {
        if (mProductDTO.getCount()>0) {
            mProductDTO.deleteProduct();
            mProductModel.updateProduct(mProductDTO);
            if (getView() != null) {
                getView().updateProductCountView(mProductDTO);
            }
        }
    }


    //endregion

    //region ======= AbstractPresenter =======

    @Override
    public void initView() {
        if (getView() != null) {
            getView().showProductView(mProductDTO);
        }
    }

    //endregion
}
